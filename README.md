# Higgs SMEFT Plots

## Setup

```
setupATLAS
cd ./PlotHelpers/ && source setup.sh; cd ../run/
```

# Plot
Note: All input data files should be in `data/`, however the config files might not be pointing to them correctly (sometimes files elsewhere in the local directory were used)... If so, check inside the `share/<plot>/config.json`.

## Correlation matrices
### STXS POIs
```
plot.py correlation_matrix -i ../share/correlation_matrix/stxs_exp.json
plot.py correlation_matrix -i ../share/correlation_matrix/stxs_obs.json
```
### STXS SMEFT fit coefficients
Note: comment out $|y_H| < 2.5$ for "model" correlation plots
```
plot.py correlation_matrix -i ../share/correlation_matrix/fit_exp.json
plot.py correlation_matrix -i ../share/correlation_matrix/fit_obs.json
```

## SMEFT acceptance effects
Note: a few "hacks" are in place inside `PlotHelpers/plots/smeft_acceptance` and `PlotHelpers/python/shape_plot.py`, must be commented out/in for HZZ/HWW cases.
```
plot.py smeft_acceptance -i ../share/smeft_acceptance/hzz_m34_cHW.dat 
plot.py smeft_acceptance -i ../share/smeft_acceptance/ggfhlvlv_mll_cHW.json 
```

## SMEFT back-propagated pTH distributions
```
plot.py smeft_pth_postfit -i ../share/smeft_pth_postfit/ev.json  ../share/smeft_pth_postfit/hyy.json
plot.py smeft_pth_postfit -i ../share/smeft_pth_postfit/ev.json  ../share/smeft_pth_postfit/hzz.json
```