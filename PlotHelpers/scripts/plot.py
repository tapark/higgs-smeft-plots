#!/usr/bin/env python

import os
import json
import types
import collections
import importlib

if __name__ == "__main__":

  # argparse
  from argparse import ArgumentParser
  parser = ArgumentParser(description="script to perform & validate lagrangian morphing")

  # perform & plot moprhing
  parser.add_argument("figure",        type=str)
  parser.add_argument("--input",  '-i', type=str, nargs='+', default=[])
  parser.add_argument("--output", '-o', type=str, default='')
  args = parser.parse_args()

  input_names = []
  inputs = collections.OrderedDict()
  for input_filepath in args.input:
    input_names.append(os.path.splitext(os.path.basename(input_filepath))[0])
    with open(input_filepath,'rb') as input_file:
      inputs.update(json.load(input_file, object_pairs_hook=collections.OrderedDict))

  if not args.output:
    args.output = args.figure +'/'+ '/'.join(input_names)

  os.makedirs(args.output,exist_ok=True)
  os.chdir(args.output)

  fig = importlib.import_module(args.figure)
  print('plot = {}'.format(args.figure))
  print('inputs = {}'.format(json.dumps(inputs,indent=2)))
  fig.plot(inputs) 
