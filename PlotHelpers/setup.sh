source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_102 x86_64-centos7-gcc8-opt

export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/2016/bin/x86_64-linux:$PATH

export PATH=$PATH:`readlink -f scripts/`
export PYTHONPATH=$PYTHONPATH:`readlink -f python/`
export PYTHONPATH=$PYTHONPATH:`readlink -f plots/`