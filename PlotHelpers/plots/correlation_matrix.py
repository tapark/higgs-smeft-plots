import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import TwoSlopeNorm, LinearSegmentedColormap
import numpy as np
import yaml

def cov_to_corr(cov):
  v = np.sqrt(np.diag(np.array(cov)))
  outer_v = np.outer(v, v)
  corr = cov / outer_v
  corr[cov == 0] = 0
  return corr

def plot(inputs):

  # mpl.rcParams['text.usetex'] = True
  # mpl.rcParams["text.latex.preamble"] = "\\usepackage{amsmath,amssymb,siunitx}"
  # mpl.rcParams["text.latex.preamble"] += "\\usepackage[alsoload=hep]{siunitx}"
  # mpl.rcParams["text.latex.preamble"] += "\\usepackage{newtxtext,newtxmath}"

  mpl.rcParams['text.usetex'] = True
  mpl.rcParams["text.latex.preamble"] = "\\usepackage{amsmath,amssymb}"
  mpl.rcParams["text.latex.preamble"] += "\\usepackage{graphicx}"
  mpl.rcParams["text.latex.preamble"] += "\\usepackage{physics}"
  mpl.rcParams["text.latex.preamble"] += "\\usepackage[alsoload=hep]{siunitx}"
  mpl.rcParams["text.latex.preamble"] += "\\sisetup{range-phrase=--}"
  mpl.rcParams["text.latex.preamble"] += "\\usepackage{helvet}"
  mpl.rcParams["text.latex.preamble"] += "\\usepackage[helvet]{sfmath}"
  mpl.rcParams["text.latex.preamble"] += "\\renewcommand{\\rmdefault}{\\sfdefault}"

  # mpl.use('pgf')
  # mpl.rcParams["pgf.texsystem"] = 'lualatex'
  # mpl.rcParams["text.latex.preamble"] += "\\usepackage{fontspec}"
  # mpl.rcParams["text.latex.preamble"] += "\\setmainfont{Helvetica}"

  # plt.rcParams["font.sans-serif"] = ["Helvetica"]
  # mpl.rcParams['font.family'] = 'sans-serif'

  # Load the correlation matrix and tick labels from the YAML file
  with open(inputs['file'], 'r') as file:
      data = yaml.safe_load(file)

  # covariance_matrix = np.array(data['covariance'])
  # correlation_matrix = cov_to_corr(covariance_matrix)
  correlation_matrix = np.array(data['correlation'])
  bin_names = data['names']
  tick_labels = ['$'+inputs['ticklabels'].get(bin_name,bin_name)+'$' for bin_name in bin_names]

  groups = list(inputs['groups'])
  group_pois = inputs['groups']
  correl_subblocks = [[None for x in range(len(groups))] for y in range(len(groups))] 
  for igroup in range(len(groups)):
    igroup_pois = group_pois[groups[igroup]]
    for jgroup in range(len(groups)):
      if inputs.get('onesided', False) and (igroup < jgroup): continue
      jgroup_pois = group_pois[groups[jgroup]]
      correl_subblock = [[None for x in range(len(jgroup_pois))] for y in range(len(igroup_pois))]
      for i,ipoi in enumerate(igroup_pois):
        for j,jpoi in enumerate(jgroup_pois):
          print(ipoi, bin_names.index(ipoi), '|', jpoi, bin_names.index(jpoi), ": corr = {}".format(correlation_matrix[bin_names.index(ipoi)][bin_names.index(jpoi)]))
          correl_subblock[i][j] = correlation_matrix[bin_names.index(ipoi)][bin_names.index(jpoi)]
          if inputs.get('onesided', False) and igroup==jgroup and i < j : correl_subblock[i][j] = 0.0
          if igroup==jgroup and i == j : correl_subblock[i][j] = 1.00
      correl_subblocks[igroup][jgroup] = correl_subblock

  # Create a figure and axes
  # Set the figure size
  # fig, ax = plt.subplots(figsize=(15,15))
  group_sizes = [len(pois) for pois in group_pois.values()]
  width_ratios, height_ratios = group_sizes+[inputs.get('cbarspace',0.0),inputs.get('cbarwidth',0.5)], group_sizes

  # if (len(groups) > 1):
  #   fig, axs = plt.subplots(nrows=len(groups), ncols=len(groups)+2, figsize=inputs.get('figsize',1)*np.array([np.sum(width_ratios),np.sum(height_ratios)]), gridspec_kw={'height_ratios':height_ratios, 'width_ratios':width_ratios}, sharex='col', sharey='row')
  # else:
  #   fig, axs = plt.subplots(nrows=len(groups), ncols=len(groups)+2, figsize=inputs.get('figsize',1)*np.array([np.sum(width_ratios),np.sum(height_ratios)]), gridspec_kw={'height_ratios':height_ratios, 'width_ratios':width_ratios})

  fig = figure(figsize=inputs.get('figsize',1)*np.array([np.sum(width_ratios),np.sum(height_ratios)]))
  gs = GridSpec(figure=fig, nrows=len(groups), ncols=len(groups)+2, height_ratios = height_ratios, width_ratios = width_ratios)
  gs.update(left=0.0, right=1.0, bottom=0.0, top=1.0)
  axs = gs.subplots(sharex='col',sharey='row')
  if (len(groups)>1):
    plt.subplots_adjust(wspace=inputs.get('wspace',0.1)/np.mean(width_ratios)*np.mean(height_ratios), hspace=inputs.get('hspace',0.1))
  else:
    plt.subplots_adjust(wspace=0,hspace=0)

  for igroup in range(len(groups)):
    igroup_pois = group_pois[groups[igroup]]
    for jgroup in range(len(groups)):
      jgroup_pois = group_pois[groups[jgroup]]

      ax = axs[igroup][jgroup] if len(groups) > 1 else axs[jgroup]
      if inputs.get('onesided',False) and igroup < jgroup: 
        ax.axis('off')
        continue

      # if igroup==jgroup:
      #   ax.set_aspect('equal')

      if not igroup==len(groups)-1:
        ax.tick_params(axis="x", bottom=False, top=False, labelbottom=False, labeltop=False)
      if jgroup:
        ax.tick_params(axis="y", left=False, right=False, labelleft=False, labelright=False)

      correl_subblock = np.array(correl_subblocks[igroup][jgroup])

      # Plot the correlation matrix as a heatmap
      norm = TwoSlopeNorm(vmin=-1, vcenter=0, vmax=1.0)
      my_gradient = LinearSegmentedColormap.from_list('my_gradient', (
                      (0.000, (0.96,0.742,0.29)),
                      (0.500, (1,1,1)),
                      (1.000, (0.1,0.32,0.738))))
      heatmap = ax.imshow(correl_subblock, norm=norm, cmap=my_gradient, aspect='auto')

      # Add correlation values in each cell
      xticklabels = []
      for j in range(correl_subblock.shape[1]):
        xticklabels.append('$'+inputs['ticklabels'][jgroup_pois[j]]+'$')
      yticklabels = []
      for i in range(correl_subblock.shape[0]):
        yticklabels.append('$'+inputs['ticklabels'][igroup_pois[i]]+'$')
      ax.set_xticks(np.arange(correl_subblock.shape[1]))
      ax.set_yticks(np.arange(correl_subblock.shape[0]))
      ax.set_xticklabels(xticklabels,fontsize=inputs['ticklabelsize'])
      ax.set_yticklabels(yticklabels,fontsize=inputs['ticklabelsize'])
      # Rotate the tick labels and set their alignment
      plt.setp(ax.get_xticklabels(), rotation=90)

      label_coord = -0.1
      if jgroup==0:
        ax.set_ylabel('$'+inputs['axislabels'][groups[igroup]]+'$',fontsize=inputs.get('axislabelsize','x-large'))
        ax.get_yaxis().set_label_coords(*inputs.get('yaxislabelcoords',(-1.0,0.5)))
      if igroup==len(groups)-1:
        ax.set_xlabel('$'+inputs['axislabels'][groups[jgroup]]+'$',fontsize=inputs.get('axislabelsize','x-large'))
        ax.get_xaxis().set_label_coords(*inputs.get('xaxislabelcoords',(0.5,-1.0)))

      for i in range(correl_subblock.shape[0]):
          for j in range(correl_subblock.shape[1]):
              # skip if too small:
              if not abs(correl_subblock[i, j]) >= inputs.get('celltextmin',0.0): continue
              # one-sided
              if igroup==jgroup and inputs.get('onesided',False) and i < j: continue
              # diagonal element & sub-block
              if igroup==jgroup and i==j:
                  ax.text(j, i, '$'+'{:.0f}'.format(correl_subblock[i, j])+'$', ha='center', va='center', color='white', fontsize=inputs.get('celltextsize','xx-small'))
              # off-diagnal sub-block & elements
              else:
                ax.text(j, i, '$'+f'{correl_subblock[i, j]:.2f}'+'$', ha='center', va='center', color='black', fontsize=inputs.get('celltextsize','xx-small'))


  # # Add colorbar
  # if len(groups) > 1:
  #   ax = axs[0][1]
  #   ax.axis('off')
  #   divider = make_axes_locatable(ax)
  #   cax = divider.append_axes("left", size="10%", pad=0.10)
  # else:

  # turn off axis for the last two columns (colorbar spacing & axis)
  if len(groups) > 1:
    for igroup in range(len(groups)):
      axs[igroup][-2].axis('off')
      axs[igroup][-1].axis('off')
  else:
    axs[-2].axis('off')
    axs[-1].axis('off')

  # make colorbar axis
  ax = fig.add_subplot(gs[:,-1])
  ax.set_aspect('auto')
  divider = make_axes_locatable(ax)
  cbar = fig.colorbar(heatmap, cax=ax, ticks=[-1.0,-0.5,0,0.5,1])
  cbar.ax.tick_params(labelsize=inputs['ticklabelsize'])
  cbar.set_label('$\\rho(\\mathrm{X},\\mathrm{Y})$', fontsize=inputs.get('axislabelsize','x-large'))

  # ATLAS caption
  if not inputs.get('onesided',False):
    ax = fig.add_subplot(gs[0,:-2])
    ax.axis('off')
    ax.set_title('$\\textbf{\\textit{ATLAS}}\\ \\ \\textrm{Preliminary}$'+(inputs.get('asimovlabel',''))+'$\\vspace{0.1em}$', loc='left', fontsize=inputs['atlaslogosize'])
    # ax.set_title('$\\sqrt{s}=\\SI{13}{\\TeV}$,\\ 139\\,$\\si{\\per\\fb}$\n$m_H = \\SI{125.09}{\GeV}$'+('\n'+inputs['smeftlabel'] if inputs.get('smeftlabel',False) else ''), loc='right', fontsize=inputs['captionsize'])
    ax.set_title('$\\sqrt{s}=\\SI{13}{\\TeV}$,\\ 139\\,$\\si{\\per\\fb}$\n$m_H = \\SI{125.09}{\GeV}$, $|y_H| < 2.5$'+('\n'+inputs['smeftlabel'] if inputs.get('smeftlabel',False) else ''), loc='right', fontsize=inputs['captionsize'])
  else:
    # put logo & captions in upper right corner
    ax = fig.add_subplot(gs[:,:-2])
    ax.axis('off')
    # plt.text(0.99,0.99,'$\\textbf{\\textit{ATLAS}}\\ \\ \\textrm{Preliminary}$'+'$\\vspace{0.1em}$\n$\\sqrt{s}=\\SI{13}{\\TeV}$,\\ 139\\,$\\si{\\per\\fb}$\n$m_H = \\SI{125.09}{\GeV}$'+('\n'+inputs['smeftlabel'] if inputs.get('smeftlabel',False) else '')+(inputs.get('asimovlabel','')),fontsize=inputs['captionsize'], transform=ax.transAxes, ha='right', va='top')
    plt.text(0.99,0.99,'$\\textbf{\\textit{ATLAS}}\\ \\ \\textrm{Preliminary}$'+'$\\vspace{0.1em}$\n$\\sqrt{s}=\\SI{13}{\\TeV}$,\\ 139\\,$\\si{\\per\\fb}$\n$m_H = \\SI{125.09}{\GeV}$, $|y_H| < 2.5$'+('\n'+inputs['smeftlabel'] if inputs.get('smeftlabel',False) else '')+(inputs.get('asimovlabel','')),fontsize=inputs['captionsize'], transform=ax.transAxes, ha='right', va='top')

  # Display the correlation matrix plot
  plt.savefig('correlation_matrix.pdf', format='pdf', bbox_inches='tight')