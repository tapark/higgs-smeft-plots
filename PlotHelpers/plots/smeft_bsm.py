
def plot(wcs=[], param='', wc_val={}, wc_label={}, process='', bin_title={}):

  import yaml
  import collections
  import numpy as np
  import math
  import ROOT
  import matplotlib.pyplot as plt
  import matplotlib.colors as colors
  from matplotlib.backends.backend_pdf import PdfPages as PDF

  # plt.rc('text', usetex = True)
  # plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \usepackage{times}')

  with open(param) as param_file:
      param = yaml.load(param_file,Loader=yaml.FullLoader)

  observables = param['observables']
  all_terms = param['coefficient terms']

  wilson_coeffcients = []
  for term in all_terms:
    if not '*' in term and term in wcs:
      wilson_coeffcients.append(term)

  squared_terms = []
  for term in all_terms:
    if '*' in term:
      c1, c2 = term.split('*')
      if c1==c2 and c1 in wcs and c2 in wcs:
        squared_terms.append(c1)

  cross_terms = []
  for term in all_terms:
    if '*' in term:
      c1, c2 = term.split('*')
      if not c1==c2 and c1 in wcs and c2 in wcs:
        cross_terms.append(term)

  bin_centers = np.arange(-0.5, len(squared_terms), 1)

  with PDF(param['name']+'.pdf','wb') as pdf_file:

    for observable in observables:

      # BSM terms matrix
      bsm_mat = np.zeros((len(squared_terms),len(squared_terms)))

      XS_sm = param['sm xsec'][observable]['val']

      for i, wc_i in enumerate(squared_terms):
        # populate diagonal cells
        B_ii = param['parameterisation'][observable][wc_i+'*'+wc_i]['val']
        bsm_mat[i][i] = B_ii * (wc_val.get(wc_i,1.0))**2

        for j, wc_j in enumerate(squared_terms):
          # populate off-diagonal cells
          B_ij = param['parameterisation'][observable]['*'.join(sorted([wc_i,wc_j]))]['val']
          bsm_mat[i][j] = B_ij * wc_val.get(wc_i,1.0) * wc_val.get(wc_j,1.0)
          # only one corner for now (symmetrical anyway)
          if not i==j:
            bsm_mat[j][i] = 0.0

      bsm_mat = bsm_mat * 100.0
      bsm_mat = np.ma.masked_where(bsm_mat == 0, bsm_mat)

      plt.set_cmap('bwr')
      vmin = -100
      vmax = +100
      # vmin = np.min(bsm_mat)
      # vmax = np.max(bsm_mat)
      norm = colors.TwoSlopeNorm(vmin=vmin, vcenter=0.0, vmax=vmax)
      
      fig = plt.figure(figsize=(10,10))
      bsm_ax = fig.add_subplot(111)
      cax = bsm_ax.matshow(bsm_mat, interpolation=None, norm=norm)
      # cbar = fig.colorbar(cax)
      # cbar.set_label("$\\sigma^{(6)}_{\\Lambda^{-4}}(c_i \\times c_j) \\slash \\sigma_{\\mathrm{SM}}\\ [\\%]$")

      bsm_ax.set_xticks(np.arange(len(squared_terms)))
      bsm_ax.set_xticklabels([wc_label.get(term,term) for term in squared_terms])
      bsm_ax.set_yticks(np.arange(len(squared_terms)))
      bsm_ax.set_yticklabels([wc_label.get(term,term) for term in squared_terms])

      for (i, j), impact in np.ndenumerate(bsm_mat):
        if i < j:
          continue
        if abs(impact) >= 0.01:
          c = '${:.2f}$'.format(impact)
        elif impact > 0.0: 
          c = '$(+)$' 
        else:
          c = '$(-)$'
        bsm_ax.text(j, i, c, va='center', ha='center', fontsize='xx-small')

      bsm_ax.set_title("$\\sigma^{(6)}_{\\Lambda^{-4}}(c_i \\times c_j) \\slash \\sigma_{\\mathrm{SM}}\\ [\\%]$", loc='left')

      bsm_ax.set_title(process, loc='right')
      plt.text(0.99,0.99, bin_title.get(observable,observable), ha='right', va='top', transform=bsm_ax.transAxes)

      # captions
      # plt.text(0.05,0.25, '$\\textbf{\\textit{ATLAS}}\\ \\textrm{Internal}$', ha='left', va='top', transform=bsm_ax.transAxes)
      # plt.text(0.05,0.20, '$\\sqrt{s} = 13\\ \\mathrm{TeV}$', ha='left', va='top', transform=bsm_ax.transAxes, fontsize='small')
      # plt.text(0.05,0.15, data.get('process',param['name']), ha='left', va='top', transform=bsm_ax.transAxes, fontsize='small')
      
      # plt.xticks(np.arange(-0.5,len(squared_terms)+0.5,1))
      bsm_ax.tick_params(axis="x", bottom=True, top=False, labelbottom=True, labeltop=False)
      plt.setp([tick.label1 for tick in bsm_ax.xaxis.get_major_ticks()], rotation=45, ha="right", va="top", rotation_mode="anchor")

      pdf_file.savefig(fig, bbox_inches='tight')
      plt.close()

