
def plot(inputs):

  import ROOT
  import numpy as np
  import PlotHelpers as ph

  histfile = ROOT.TFile.Open(inputs['filepath'],'read')

  hist_sm = histfile.Get(inputs['sm_samplename']+'/'+inputs['histname']) 
  hist_smeft_pos = histfile.Get(inputs['smeft_pos_samplename']+'/'+inputs['histname'])
  hist_smeft_neg = histfile.Get(inputs['smeft_neg_samplename']+'/'+inputs['histname'])

  # hack job for hzz
  hist_sm_file = ROOT.TFile("/project/6001378/thpark/hcomb/eft-acceptance-top-scheme/data/non_normalised/cHWB_0_cHB_0_cHW_0.root")
  hist_smeft_pos_file = ROOT.TFile("/project/6001378/thpark/hcomb/eft-acceptance-top-scheme/data/non_normalised/cHWB_0_cHB_0_cHW_1.root")
  hist_smeft_neg_file = ROOT.TFile("/project/6001378/thpark/hcomb/eft-acceptance-top-scheme/data/non_normalised/cHWB_0_cHB_0_cHW_m1.root")
  hist_sm = hist_sm_file.Get("ATLAS_2020_I1790439/m34")
  hist_smeft_pos = hist_smeft_pos_file.Get("ATLAS_2020_I1790439/m34")
  hist_smeft_neg = hist_smeft_neg_file.Get("ATLAS_2020_I1790439/m34")

  # disentangle int & bsm
  hist_int = hist_smeft_pos.Clone()
  hist_int.Add(hist_smeft_neg,-1)
  hist_int.Scale(1/2.0)
  hist_lmbd2 = hist_sm.Clone()
  hist_lmbd2.Add(hist_int)

  # normalize to shapes
  # hist_sm.Scale(1.0/hist_sm.Integral('width'))
  # hist_lmbd2.Scale(1.0/hist_lmbd2.Integral('width'))
  # hist_smeft_pos.Scale(1.0/hist_smeft_pos.Integral('width'))

  fig = ph.Figure(800,800)

  h_tmp = hist_sm.Clone()
  h_tmp.Scale(1.0/h_tmp.Integral('width'))
  shape_ax = ph.ShapePlot(xmin = inputs['xmin'], xmax=inputs['xmax'], ymin=0.0, ymax=h_tmp.GetMaximum()*1.75, rmin=0.5, rmax=2.25, subfrac=1/3.)
  # shape_ax = ph.ShapePlot(xmin = inputs['xmin'], xmax=inputs['xmax'], ymin=0.0, ymax=h_tmp.GetMaximum()*1.75, rmin=0.85, rmax=1.175, subfrac=1/3.)
  shape_ax.xtitle(inputs['xtitle'], offset=1.5)
  shape_ax.ytitle('Normalized to SM [a.u.]', offset=2.0)
  shape_ax.rtitle('SMEFT / SM', offset=2.0)
  shape_ax.rticks(primary=5, secondary=5)

  sm_plot = ph.Prediction(hist_sm, label=inputs['sm_process'], fill={'color':ROOT.kGray, 'alpha':0.5})
  lmbd2_plot = ph.Prediction(hist_lmbd2, label='{} = {:g}, up to \\Lambda^{{-2}}'.format(inputs['wclabel'], inputs['wcval']), line={'color':ROOT.kGreen+1})
  lmbd4_plot = ph.Prediction(hist_smeft_pos, label='{} = {:g}, up to \\Lambda^{{-4}}'.format(inputs['wclabel'], inputs['wcval']), line={'color':ROOT.kViolet+10, 'style':ROOT.kDashed})

  shape_ax.set_baseline(sm_plot)
  shape_ax.add_comparison(lmbd2_plot)
  shape_ax.add_comparison(lmbd4_plot)

  xcutlines = []
  for xcut in inputs['xcuts']:
    main_xcutline = ROOT.TGraph(2, np.array([xcut]*2), np.array([0.0, h_tmp.GetMaximum()*0.9]))
    ratio_xcutline = ROOT.TGraph(2, np.array([xcut]*2), np.array([0.0,10.0]))
    main_xcutline = ph.Line(main_xcutline, line={'color': ROOT.kGray+2, 'style':ROOT.kDotted})
    ratio_xcutline = ph.Line(ratio_xcutline, line={'color': ROOT.kGray+2, 'style':ROOT.kDotted})
    shape_ax.add_main(main_xcutline)
    shape_ax.add_sub(ratio_xcutline)

  leg = ph.Legend(625,650,height=150)
  leg.add_entry(sm_plot)
  leg.add_entry(lmbd2_plot)
  leg.add_entry(lmbd4_plot)

  fig.save(shape_ax, leg, name='smeft_acceptance')

  