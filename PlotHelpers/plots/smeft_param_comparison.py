
def plot(inputs, baseline, comparison):

  import yaml
  import collections
  import numpy as np
  import ROOT

  input_numerator = inputs[comparison]
  input_denominator = inputs[baseline]

  with open(input_numerator) as a:
    smeft_numerator = yaml.load(a)
  with open(input_denominator) as b:
    smeft_denominator = yaml.load(b)

  categories = sorted(smeft_numerator['observables'])
  ncats = len(categories)

  coefficients = smeft_numerator['coefficient terms']

  hist_numerator = ROOT.TH1F('','',ncats,0,ncats)
  hist_denominator = ROOT.TH1F('','',ncats,0,ncats)

  import rtplot as plt
  fig = plt.Figure(800,600)
  fig.open('param_comparison')

  for term in coefficients:

    if not term in smeft_denominator['coefficient terms']: continue

    for icat, cat in enumerate(categories):

      if not cat in smeft_numerator['parameterisation']:
        hist_denominator.SetBinContent(icat+1,1.0)
        hist_denominator.SetBinError(icat+1,1.0)
        continue

      if 'parameterisation' in smeft_numerator:
        aval, aerr = smeft_numerator['parameterisation'][cat][term]['val'], smeft_numerator['parameterisation'][cat][term]['err']
      else:
        aval, aerr = smeft_numerator['parameterisation'][cat][term]['val'], smeft_numerator['parameterisation'][cat][term]['err']
      hist_numerator.SetBinContent(icat+1,aval)
      hist_numerator.SetBinError(icat+1,aerr)

      if 'parameterisation' in smeft_denominator:
        bval, berr = smeft_denominator['parameterisation'][cat][term]['val'], smeft_denominator['parameterisation'][cat][term]['err']
      else:
        bval, berr = smeft_denominator['parameterisation'][cat][term]['val'], smeft_denominator['parameterisation'][cat][term]['err']
      hist_denominator.SetBinContent(icat+1,bval)
      hist_denominator.SetBinError(icat+1,berr)

    param_numerator = plt.MC(hist_numerator, line={'color': ROOT.kRed}, fill={'color':ROOT.kRed, 'alpha':0.20})
    param_denominator = plt.MC(hist_denominator, line={'color': ROOT.kBlack}, fill={'color':ROOT.kBlack, 'alpha':0.20})

    print(hist_denominator.GetMinimum()*(0.5 if hist_denominator.GetMinimum()>0 else 1.5))
    print(hist_denominator.GetMaximum()*(1.5 if hist_denominator.GetMaximum()>0 else 0.5))
    comparison = plt.Ratio(xbins = categories, ymin=hist_denominator.GetMinimum()*(0.0 if hist_denominator.GetMinimum()>0 else 1.5), ymax=hist_denominator.GetMaximum()*(1.5 if hist_denominator.GetMaximum()>0 else 0.0), rmin=0.5, rmax=1.5)
    # comparison = plt.Ratio(xbins = categories, ymin=-2.0, ymax=2.0, rmin=0.5, rmax=1.5)
    comparison.set_denominator(param_denominator)
    comparison.add_numerator(param_numerator)
    comparison.ytitle('A_{{i}}({coeff})'.format(coeff=term), offset=2.5)
    comparison.rtitle('standalone / reweight', offset=2.5)

    fig.save(comparison)

  fig.close()