import json, yaml
import collections
import itertools

import ROOT
import math
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.ticker import AutoMinorLocator
from matplotlib.backends.backend_pdf import PdfPages as PDF
from matplotlib.gridspec import GridSpec
from matplotlib import rc  

def compute_br_impact(wc_val, channel_int, width_int, channel_bsm = 0.0, width_bsm = 0.0):
  return (1 + channel_int*wc_val + channel_bsm*wc_val**2)/(1 + width_int*wc_val + width_bsm*wc_val**2)-1

def plot(inputs):

  mpl.rcParams["text.usetex"] = True

  # Timew New Roman
  # mpl.rcParams["text.latex.preamble"] = "\\usepackage{amsmath,amssymb,siunitx,newtxtext,newtxmath}"
  # Helvetica for ATLAS logo
  # mpl.rcParams["font.family"] = 'sans-serif'
  # mpl.rcParams['font.sans-serif'] = ['Helvetica']

  # Helvetica
  mpl.rcParams["text.latex.preamble"] = "\\usepackage{amsmath,amssymb,siunitx,newtxtext,newtxmath}"
  mpl.rcParams["text.latex.preamble"] += "\\usepackage{helvet}"
  mpl.rcParams["text.latex.preamble"] += "\\usepackage[helvet]{sfmath}"
  # mpl.rcParams["text.latex.preamble"] += "\\usepackage{sansmathfonts}"
  mpl.rcParams["text.latex.preamble"] += "\\renewcommand{\\rmdefault}{\\sfdefault}"

  # DejaVu Sans (NOT WORKING unfortunately)
  # mpl.rcParams["text.latex.preamble"] = "\\usepackage{amsmath,amssymb,siunitx,DejaVuSans}\\renewcommand*\\familydefault{\\sfdefault}\\usepackage[T1]{fontenc}"
  # mpl.rcParams['font.family'] = 'sans-serif'
  # mpl.rcParams['font.sans-serif'] = ['DejaVuSans']
  # mpl.rcParams['font.serif'] = ['DejaVuSans']

  # use LaTeX
  # plt.rc('text', usetex = True)
  # plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \usepackage{times}')

  # grid lines between major ticks
  # minor_locator = AutoMinorLocator(2)
  # plt.gca().xaxis.set_minor_locator(minor_locator)
  # plt.grid(which='minor', linestyle='-')

  nrows = len(inputs['impact_rows']) + 1 # extra for stat unc row
  all_coefficients = []
  for row, coefficients in enumerate(inputs['impact_rows']): all_coefficients += coefficients

  # compute uniform bar widths across all rows
  impact_total_binwidth = 0.9
  impact_barwidth = impact_total_binwidth / max([len(coefficients) for coefficients in inputs['row_coefficients'].values()])
  impact_linewidth = 1.0

  all_processes = inputs['production_modes'] + inputs['decay_channels']
  has_decay = len(inputs['decay_channels'])

  process_param = {}
  param_file_path = inputs['param_file']
  with open(param_file_path) as pf: 
    param_yaml = yaml.load(pf,Loader=yaml.FullLoader)
    for proc in all_processes:
      process_param[proc] = param_yaml

  for proc in all_processes:

    # add merged observables
    for iobs, obs in enumerate(inputs['process_observables'][proc]):

      if obs in inputs.get('merged_observables',{}):
        merged_obs = obs
        obslist = inputs['merged_observables'][merged_obs]

        # initialize
        process_param[proc]['sm xsec'][merged_obs] = {'val' : 0.0}
        process_param[proc]['parameterisation'][merged_obs] = {}
        for term in process_param[proc]['coefficient terms']:
          process_param[proc]['parameterisation'][merged_obs][term] = {'val' : 0.0}

        # merge sm xsec
        merged_sm_xsec = np.sum([process_param[proc]['sm xsec'][obs_to_merge]['val'] for obs_to_merge in obslist])
        process_param[proc]['sm xsec'][merged_obs]['val'] = merged_sm_xsec

        # merge param
        for term in process_param[proc]['coefficient terms']:
          merged_param_val = np.sum([process_param[proc]['sm xsec'][obs_to_merge]['val']*process_param[proc]['parameterisation'][obs_to_merge].get(term,{}).get('val',0.0) for obs_to_merge in obslist]) / merged_sm_xsec
          process_param[proc]['parameterisation'][merged_obs][term]['val'] = merged_param_val


  obs_smrelerr = {}
  with open(inputs['error_file']) as ef:
    smrelerr = json.load(ef)
    ypars = smrelerr['ypars']
    for iobs, obs in enumerate([obs for obslist in inputs['process_observables'].values() for obs in obslist]):
      if obs in ypars:
        obs_smrelerr[obs] = smrelerr['matrix'][ypars.index(obs)][0]
      else:
        obs_smrelerr[obs] = 100

  # compute binnings
  # first, scan over columns of production modes
  production_figwidth = 0.0
  production_bincenter = 0.0
  production_bincenters = []
  production_binlabels = []
  production_separators = []
  for iprod, prod in enumerate(inputs['production_modes']):
    prod_observables = inputs['process_observables'].get(prod,process_param[prod]['observables'])
    for iobs, obs in enumerate(prod_observables):
      production_bincenters.append(production_bincenter)
      production_binlabels.append(inputs['observable_label'].get(obs,obs))
      production_bincenter += 1
    production_separators.append(production_bincenter)
    production_bincenter += 1
    production_figwidth += len(prod_observables)
  decay_figwidth = 0.0
  decay_bincenter = 0.0
  decay_bincenters = []
  decay_binlabels = []
  decay_separators = []
  # same for decay
  for idec, decay in enumerate(inputs['decay_channels']):
    dec_observables = inputs['process_observables'].get(decay,process_param[decay]['observables'])
    for iobs, obs in enumerate(dec_observables):
      decay_bincenters.append(decay_bincenter)
      decay_binlabels.append(inputs['observable_label'].get(obs,obs))
      decay_bincenter += 1
    decay_separators.append(decay_bincenter)
    decay_bincenter += 1
    decay_figwidth += len(dec_observables)
  # dummy bin exists between each process column
  production_figwidth += len(inputs['production_modes'])-1
  decay_figwidth += len(inputs['decay_channels'])-1

  # processes = [proc for proc in [*l for l in data['processes'].keys()]]

  with PDF('smeft_impact.pdf','wb') as pdf_file:
    if has_decay:
      fig, axs = plt.subplots(nrows, 2, figsize=(18,18), gridspec_kw = {'width_ratios': [production_figwidth, decay_figwidth]}, sharex='col', sharey=False, constrained_layout=True)
    else:
      fig, axs = plt.subplots(nrows, 1, figsize=(18,18), sharex='col', sharey=False, constrained_layout=True)
    fig.set_constrained_layout_pads(w_pad=1e-5, wspace=1e-5, h_pad=1e-5, hspace=1e-5)

    # stat unc row
    unc_axs = axs[0]
    prod_ax = unc_axs[0] if has_decay else unc_axs
    decay_ax = unc_axs[1] if has_decay else None
    # unc_axs[0].set_ylabel('$\\mathrm{SM}\\ \\Delta\\sigma / \\sigma$')
    prod_ax.set_ylabel('$\\mathrm{Rel.\\ unc.}$', fontsize='x-large')
    # production-side
    prod_ax.tick_params(axis="x", bottom=False, top=False, labelbottom=False, labeltop=False)
    prod_ax.tick_params(axis="y", left=True, right=True, labelleft=True, labelright=False, direction='in', labelsize='large')
    prod_ax.set_xlim([-0.5, production_bincenters[-1]+0.5])
    prod_ax.vlines(x=np.arange(0.5,production_bincenters[-1],1), ymin=-200, ymax=200, linewidth=0.5, color='lightgrey', linestyle='solid', zorder=-1)
    prod_smrelunc = []
    for iprod, prod in enumerate(inputs['production_modes']):
      prod_observables = inputs['process_observables'].get(prod,process_param[prod]['observables'])
      for iobs, obs in enumerate(prod_observables):
        prod_smrelunc.append(obs_smrelerr[obs])
    prod_smrelunc = np.array(prod_smrelunc)
    prod_ax.bar(np.array(production_bincenters), prod_smrelunc, width=impact_total_binwidth, linewidth=0.5, edgecolor='grey', hatch='////', color='none', zorder=0)
    # prod_ax.step(np.array(production_bincenters)+0.5, prod_smrelunc, linewidth=0.5, color='lightgrey', zorder=1)
    for iprod, prod_sep in enumerate(production_separators):
      prod_ax.fill_between([prod_sep-0.5,prod_sep+0.5], +1e3, -1e3, color='lightgrey', alpha=0.2)
    # decay-side
    # decay_ax.set_ylabel('$\\mathrm{SM}\\ \\Delta\\Gamma / \\Gamma$')

    if has_decay: 
      print("hm")
      decay_ax.set_ylabel('')
      decay_ax.yaxis.set_label_position('right')
      decay_ax.tick_params(axis="x", bottom=False, top=False, labelbottom=False, labeltop=False)
      decay_ax.tick_params(axis="y", left=True, right=True, labelleft=False, labelright=True, direction='in', labelsize='large')
      decay_ax.set_xlim([-0.5, decay_bincenters[-1]+0.5])
      decay_ax.vlines(x=np.arange(0.5,decay_bincenters[-1],1), ymin=-200, ymax=200, linewidth=0.5, color='lightgrey', linestyle='solid', zorder=-1)
      decay_smrelunc = []
      for idec, decay in enumerate(inputs['decay_channels']):
        decay_observables = inputs['process_observables'].get(decay,process_param[decay]['observables'])
        for iobs, obs in enumerate(decay_observables):
          decay_smrelunc.append(obs_smrelerr[obs])
      decay_smrelunc = np.array(decay_smrelunc)
      decay_ax.bar(np.array(decay_bincenters), decay_smrelunc, width=impact_total_binwidth, linewidth=0.5, edgecolor='grey', hatch='////', color='none', zorder=0)
      # decay_ax.step(np.array(decay_bincenters)+0.5, decay_smrelunc, linewidth=0.5, color='lightgrey', zorder=1)
      for icol, unc_ax in enumerate(unc_axs):
        unc_ax.set_ylim(bottom = 0.0, top = 1.0)
        unc_ax.set_yticks([0.0, 0.5, 1.0])
      for idecay, dec_sep in enumerate(decay_separators):
        decay_ax.fill_between([dec_sep-0.5, dec_sep+0.5], +1e3, -1e3, color='lightgrey', alpha=0.2)

    # ATLAS labels
    prod_ax.set_title("$\\textbf{\\textit{ATLAS}}$ Preliminary$\\ \\ {\\sqrt{s}=\\SI{13}{\\TeV},\\ \\SI{139}{\\femto\\barn\\tothe{-1}}}$", loc='left', fontsize='xx-large')

    for irow, row in enumerate(inputs['impact_rows']):
      coefficients = inputs['row_coefficients'][row]

      # first, production modes
      prod_ax = axs[irow+1][0] if has_decay else axs[irow+1]
      prod_ax.yaxis.set_label_position('left')
      if irow==0: prod_ax.set_ylabel(inputs['impact_ylabel'][0], fontsize='x-large')
      prod_ax.tick_params(axis="x", bottom=False, top=False, labelbottom=False, labeltop=False)
      prod_ax.tick_params(axis="y", left=True, right=False, labelleft=True, labelright=False, direction='in', labelsize='large')
      prod_ax_ymax = inputs['impact_ymax'].get(row,[100,100])[0]
      prod_ax.set_ylim(bottom=-prod_ax_ymax*0.2, top=1.1*prod_ax_ymax)
      prod_ax.set_yticks([-prod_ax_ymax*0.1, prod_ax_ymax])
      if irow==nrows-2:
        prod_ax.tick_params(axis="x", bottom=False, top=False, labelbottom=True, labeltop=False)
        prod_ax.set_xlim([-0.5, production_bincenters[-1]+0.5])
        prod_ax.set_xticks(production_bincenters)
        prod_ax.set_xticklabels(production_binlabels)
        plt.setp([tick.label1 for tick in prod_ax.xaxis.get_major_ticks()], rotation=60, ha="right", va="top", rotation_mode="anchor", fontsize='large')

      for icoeff, coeff in enumerate(coefficients):
        prod_barcenters = np.array(production_bincenters)-(impact_barwidth*len(coefficients)/2.0)+(impact_barwidth/2.0)+(icoeff*impact_barwidth)

        prod_coeff_int = []
        prod_coeff_int_bsm = []
        for iprod, prod in enumerate(inputs['production_modes']):
          prod_observables = inputs['process_observables'].get(prod,process_param[prod]['observables'])
          for iobs, obs in enumerate(prod_observables):
            linear_impact = process_param[prod]['parameterisation'][obs].get(coeff,{}).get('val',0.0)*inputs['coefficient_value'].get(coeff,1.0)
            quadratic_impact = process_param[prod]['parameterisation'][obs].get('*'.join([coeff,coeff]),{}).get('val',0.0)*(inputs['coefficient_value'].get(coeff,1.0)**2)
            quadratic_impact += linear_impact
            prod_coeff_int.append(linear_impact)
            prod_coeff_int_bsm.append(quadratic_impact)

        prod_coeff_int = np.array(prod_coeff_int)
        prod_coeff_int_bsm = np.array(prod_coeff_int_bsm)

        if inputs.get('bsm',False):
          # filter out zero-impacts to clean up colors for linear impact
          prod_barcenters_nonzero = np.array([ ele[0] for ele in list(filter(lambda c: c[1]!=0.0, list(zip(prod_barcenters, prod_coeff_int)))) ])
          prod_coeff_int_bsm_nonzero = np.array([ ele[0] for ele in list(filter(lambda c: c[1]!=0.0, list(zip(prod_coeff_int_bsm, prod_coeff_int)))) ])
          prod_ax.bar(prod_barcenters, prod_coeff_int, width=impact_barwidth, facecolor=inputs['coefficient_colors'][icoeff], alpha=1/3., edgecolor='none', zorder=1)
          prod_ax.bar(prod_barcenters_nonzero, prod_coeff_int_bsm_nonzero, width=impact_barwidth, linewidth=impact_linewidth, facecolor='none', edgecolor=inputs['coefficient_colors'][icoeff], zorder=3)
          # dummy for legend
          prod_ax.bar([-10], [-10], width=impact_barwidth, linewidth=impact_linewidth, facecolor='none', edgecolor=inputs['coefficient_colors'][icoeff], label='${} = {:g}$'.format(inputs['coefficient_label'].get(coeff,coeff), inputs['coefficient_value'].get(coeff,1.0)))
        else:
          prod_ax.bar(prod_barcenters, prod_coeff_int, width=impact_barwidth, color=inputs['coefficient_colors'][icoeff], label='${} = {:g}$'.format(inputs['coefficient_label'].get(coeff,coeff), inputs['coefficient_value'].get(coeff,1.0)), zorder=1)

      # separator lines & fills between bins & processes
      prod_ax.vlines(x=np.arange(0.5,production_bincenters[-1],1), ymin=-200, ymax=200, linewidth=0.5, color='lightgrey', linestyle='solid')
      for iprod, prod_sep in enumerate(production_separators):
        prod_ax.hlines(y=[0.0], linewidth=0.5, xmin=0.5+(production_separators[iprod-1] if iprod else -1.0), xmax=-0.5+production_separators[iprod], color='black', linestyle='dashed', zorder=5)
        prod_ax.fill_between([prod_sep-0.5,prod_sep+0.5], +1e3, -1e3, color='lightgrey', alpha=0.2)

      # legend between STXS & BR
      prod_ax.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), framealpha=0.0, fontsize='medium')

      if has_decay:
        dec_ax = axs[irow+1][1]
        if irow==0: dec_ax.set_ylabel(inputs['impact_ylabel'][1], fontsize='x-large')
        dec_ax.yaxis.set_label_position('right')
        dec_ax.tick_params(axis="x", bottom=False, top=False, labelbottom=False, labeltop=False)
        dec_ax.tick_params(axis="y", left=False, right=True, labelleft=False, labelright=True, direction='in', labelsize='large')
        dec_ax_ymax = inputs['impact_ymax'].get(row,[100,100])[1]
        dec_ax.set_ylim(bottom=-dec_ax_ymax*0.2, top=1.1*dec_ax_ymax)
        dec_ax.set_yticks([-dec_ax_ymax*0.1, dec_ax_ymax])
        if irow==nrows-2:
          dec_ax.tick_params(axis="x", bottom=False, top=False, labelbottom=True, labeltop=False)
          dec_ax.set_xlim([-0.5, decay_bincenters[-1]+0.5])
          dec_ax.set_xticks(decay_bincenters)
          dec_ax.set_xticklabels(decay_binlabels)
          plt.setp([tick.label1 for tick in dec_ax.xaxis.get_major_ticks()], rotation=-60, ha="left", va="top", rotation_mode="anchor", fontsize='large')

        for icoeff, coeff in enumerate(coefficients):
          dec_barcenters = np.array(decay_bincenters)-(impact_barwidth*len(coefficients)/2.0)+(impact_barwidth/2.0)+(icoeff*impact_barwidth)

          dec_coeff_int = np.array([])
          dec_coeff_bsm = np.array([])
          tot_coeff_int = np.array([])
          tot_coeff_bsm = np.array([])

          # compute impact on BR
          coeff_val = inputs['coefficient_value'].get(coeff,1.0)
          print(coeff, coeff_val)
          for idec, decay in enumerate(inputs['decay_channels']):
            dec_observables = inputs['process_observables'].get(decay,process_param[decay]['observables'])
            for iobs, obs in enumerate(dec_observables):

              # linear_impact = process_param[decay]['parameterisation'][obs].get(coeff,{}).get('val',0.0)*inputs['coefficient_value'].get(coeff,1.0)
              # quadratic_impact = linear_impact + process_param[decay]['parameterisation'][obs].get('*'.join([coeff,coeff]),{}).get('val',0.0)*(inputs['coefficient_value'].get(coeff,1.0)**2)

              ch_int_impact = process_param[decay]['parameterisation'][obs].get(coeff,{}).get('val',0.0)
              ch_bsm_impact = process_param[decay]['parameterisation'][obs].get('*'.join([coeff,coeff]),{}).get('val',0.0)

              tot_int_impact = process_param[decay]['parameterisation']["HTOT"].get(coeff,{}).get('val',0.0)
              tot_bsm_impact = process_param[decay]['parameterisation']["HTOT"].get('*'.join([coeff,coeff]),{}).get('val',0.0)

              dec_coeff_int = np.append(dec_coeff_int,ch_int_impact)
              dec_coeff_bsm = np.append(dec_coeff_bsm,ch_bsm_impact)
              tot_coeff_int = np.append(tot_coeff_int,tot_int_impact)
              tot_coeff_bsm = np.append(tot_coeff_bsm,tot_bsm_impact)

          print(dec_coeff_int)
          print(tot_coeff_int)

          br_int_impact = compute_br_impact(coeff_val, dec_coeff_int, tot_coeff_int)
          br_bsm_impact = compute_br_impact(coeff_val, dec_coeff_int, tot_coeff_int, dec_coeff_bsm, tot_coeff_bsm)

          print(br_int_impact)
          print('-----')

          dec_coeff_int = br_int_impact
          dec_coeff_int_bsm = br_bsm_impact

          if inputs.get('bsm',False):
            # filter out zero-impacts to clean up colors for linear impact
            dec_barcenters_linear_nonzero = np.array([ ele[0] for ele in list(filter(lambda c: c[1]!=0.0, list(zip(dec_barcenters, dec_coeff_int)))) ])
            dec_coeff_lin_nonzero = np.array(list(filter(lambda impact: impact!=0.0, dec_coeff_int)))
            dec_barcenters_quad_nonzero = np.array([ ele[0] for ele in list(filter(lambda c: c[1]!=0.0, list(zip(dec_barcenters, dec_coeff_int_bsm)))) ])
            dec_coeff_quad_nonzero = np.array(list(filter(lambda impact: impact!=0.0, dec_coeff_int_bsm)))
            dec_ax.bar(dec_barcenters_linear_nonzero, dec_coeff_lin_nonzero, width=impact_barwidth, linewidth=0.0,              facecolor=inputs['coefficient_colors'][icoeff], alpha=1/3., edgecolor='none', zorder=1)
            dec_ax.bar(dec_barcenters_quad_nonzero, dec_coeff_quad_nonzero, width=impact_barwidth, linewidth=impact_linewidth, facecolor='none', edgecolor=inputs['coefficient_colors'][icoeff], label='${} = {:g}$'.format(inputs['coefficient_label'].get(coeff,coeff), inputs['coefficient_value'].get(coeff,1.0)), zorder=3)
          else:
            dec_ax.bar(dec_barcenters, dec_coeff_int, width=impact_barwidth, color=inputs['coefficient_colors'][icoeff], label='${} = {:g}$'.format(inputs['coefficient_label'].get(coeff,coeff), inputs['coefficient_value'].get(coeff,1.0)), zorder=1)


        # separator lines & fills between bins & processes
        dec_ax.vlines(x=np.arange(0.5,decay_bincenters[-1],1), ymin=-200, ymax=200, linewidth=0.5, color='lightgrey', linestyle='solid')
        for idecay, dec_sep in enumerate(decay_separators):
          dec_ax.hlines(y=[0.0], linewidth=0.5, xmin=0.5+(decay_separators[idecay-1] if idecay else -1.0), xmax=-0.5+decay_separators[idecay], color='black', linestyle='dashed', zorder=5)
          dec_ax.fill_between([dec_sep-0.5, dec_sep+0.5], +1e3, -1e3, color='lightgrey', alpha=0.2)

    pdf_file.savefig(fig, bbox_inches='tight')
    plt.close()