
def plot(data):

  import yaml
  import collections
  import numpy as np
  import math
  import ROOT
  import matplotlib.pyplot as plt
  import matplotlib.colors as colors
  from matplotlib.backends.backend_pdf import PdfPages as PDF

  plt.rc('text', usetex = True)
  plt.rc('text.latex', preamble=r'\usepackage{amsmath} \usepackage{amssymb} \usepackage{times}')

  wc_val = data.get('wc_val',{})
  with open(data['param']) as param_file:
      param = yaml.load(param_file,Loader=yaml.FullLoader)

  wc_label = data.get('wc_label',{})

  observables = param['observables']
  all_terms = param['coefficient terms']

  wilson_coefficients = data['wcs']
  # for term in all_terms:
  #   if not '*' in term:
  #     wilson_coefficients.append(term)

  bin_centers = np.arange(-0.5, len(wilson_coefficients), 1)

  with PDF(param['name']+'.pdf','wb') as pdf_file:

    fig, axes = plt.subplots(len(wilson_coefficients),1, figsize=(20,10), sharex=True)

    for iwc, wc in enumerate(wilson_coefficients):

      int_mat = np.zeros((1,len(observables)))
      for iobs,observable in enumerate(observables):
        XS_sm = param['sm xsec'][observable]['val']
        A_i = param['parameterisation'][observable][wc]['val']
        int_mat[0][iobs] = A_i * (wc_val.get(wc,1.0))

        # BSM terms matrix

      int_mat = int_mat * 100.0
      int_mat = np.ma.masked_where(int_mat == 0, int_mat)

      plt.set_cmap('bwr')
      vmin = -100
      vmax = +100
      # vmin = np.min(int_mat)
      # vmax = np.max(int_mat)
      norm = colors.TwoSlopeNorm(vmin=vmin, vcenter=0.0, vmax=vmax)
      
      cax = axes[iwc].matshow(int_mat, interpolation=None, norm=norm)

      # only for bottom row
      axes[iwc].tick_params(axis="x", bottom=False, top=False, labelbottom=False, labeltop=False)
      axes[iwc].set_yticks([0.0])
      axes[iwc].set_yticklabels([data.get('wc_label',{}).get(wc,wc)])

      for (i, j), impact in np.ndenumerate(int_mat):
        if i > j:
          continue
        if abs(impact) >= 0.01:
          c = '${:.2f}$'.format(impact)
        elif impact > 0.0: 
          c = '$(+)$' 
        else:
          c = '$(-)$'
        axes[iwc].text(j, i, c, va='center', ha='center', fontsize='xx-small')

      # captions
      # plt.text(0.05,0.25, '$\\textbf{\\textit{ATLAS}}\\ \\textrm{Preliminary}$', ha='left', va='top', transform=int_ax.transAxes)
      # plt.text(0.05,0.20, '$\\sqrt{s} = 13\\ \\mathrm{TeV}$', ha='left', va='top', transform=int_ax.transAxes, fontsize='small')
      # plt.text(0.05,0.15, data.get('process',param['name']), ha='left', va='top', transform=int_ax.transAxes, fontsize='small')
      # plt.text(0.05,0.10, data.get('bin_title',{}).get(observable,observable), ha='left', va='top', transform=int_ax.transAxes, fontsize='small')
      
      # plt.xticks(np.arange(-0.5,len(wilson_coefficients)+0.5,1))

    # cbar = fig.colorbar(cax, ax=axes.ravel().tolist(), fraction=0.05, pad=0.01, aspect=50)
    # cbar.set_label("$\\sigma^{(6)}_{\\Lambda^{-2}}(c_i) \\slash \\sigma_{\\mathrm{SM}}\\ [\\%]$")

    # only for top row
    axes[0].set_title("$\\sigma^{(6)}_{\\Lambda^{-2}}(c_i) \\slash \\sigma_{\\mathrm{SM}}\\ [\\%]$", loc='left')
    axes[0].set_title(data['process'], loc='center')

    # only for bottom row
    axes[-1].set_xticks(np.arange(0, len(observables), 1))
    axes[-1].set_xticklabels([data.get('bin_title', {}).get(obs,obs) for obs in observables])
    axes[-1].tick_params(axis="x", bottom=True, top=False, labelbottom=True, labeltop=False)
    plt.setp([tick.label1 for tick in axes[-1].xaxis.get_major_ticks()], rotation=45, ha="right", va="top", rotation_mode="anchor")
    # axes[-1].set_xlabel(data['process'])


    # fig.suptitle(data['process'])

    pdf_file.savefig(fig, bbox_inches='tight')
    plt.close()

