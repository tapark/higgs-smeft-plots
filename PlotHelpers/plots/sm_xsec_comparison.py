
def plot(baseline, comparisons):

  import yaml
  import collections
  import numpy as np
  import ROOT

  process = settings['process']

  name_numerator = settings['numerator']
  name_denominator = settings['denominator']

  input_numerator = inputs[settings['numerator']]
  input_denominator = inputs[settings['denominator']]

  with open(input_numerator) as a:
    smeft_numerator = yaml.load(a)
  with open(input_denominator) as b:
    smeft_denominator = yaml.load(b)

  categories = sorted(smeft_numerator['observables'])
  ncats = len(categories)

  hist_numerator = ROOT.TH1F('','',ncats,0,ncats)
  hist_denominator = ROOT.TH1F('','',ncats,0,ncats)

  import rtplot as plt
  fig = plt.Figure(800,600)

  for icat, cat in enumerate(categories):

    if not cat in smeft_numerator['parameterisation']:
      hist_denominator.SetBinContent(icat+1,1.0)
      hist_denominator.SetBinError(icat+1,1.0)
      continue

    aval, aerr = smeft_numerator['sm xsec'][cat]['val'], smeft_numerator['sm xsec'][cat]['err']
    hist_numerator.SetBinContent(icat+1,aval)
    hist_numerator.SetBinError(icat+1,aerr)

    bval, berr = smeft_denominator['sm xsec'][cat]['val'], smeft_denominator['sm xsec'][cat]['err']
    hist_denominator.SetBinContent(icat+1,bval)
    hist_denominator.SetBinError(icat+1,berr)

  param_numerator = plt.MC(hist_numerator, line={'color': ROOT.kRed}, fill={'color':ROOT.kRed, 'alpha':0.20})
  param_denominator = plt.MC(hist_denominator, line={'color': ROOT.kBlack}, fill={'color':ROOT.kBlack, 'alpha':0.20})

  ymin = (hist_denominator.GetMinimum()*(0.0 if hist_denominator.GetMinimum()>0 else 2.0))
  ymax = (hist_denominator.GetMaximum()*(2.0 if hist_denominator.GetMaximum()>0 else 0.0))
  comparison = plt.Ratio(xbins = categories, ymin=ymin, ymax=ymax, rmin=0.0, rmax=2.0)
  comparison.set_denominator(param_denominator)
  comparison.add_numerator(param_numerator)
  comparison.ytitle(settings['title']['xs'].format(), offset=2.0)
  comparison.rtitle(settings['title']['ratio'].format(num=settings['numerator'],den=settings['denominator']),offset=2.0)

  fig.save(comparison, '{proc}_sm_{numer}_vs_{denom}'.format(proc=process,numer=name_numerator,denom=name_denominator), ext='pdf')