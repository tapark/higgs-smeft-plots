import ROOT
import math
import numpy as np

data_color = 'black'
smeft_color = 'blue'
sm_color = 'red'
smerr_color = 'lightgrey'

def get_chi2(exp_vals,obs_vals,obs_errs):
  chi2 = 0.0
  for i,exp_val in enumerate(exp_vals):
    chi2 += (exp_val - obs_vals[i])**2 / obs_errs[i]**2
  return chi2

def plot(inputs):

  import yaml
  import collections
  import itertools

  import matplotlib as mpl
  import matplotlib.pyplot as plt
  import matplotlib.colors as colors
  from matplotlib.ticker import AutoMinorLocator
  from matplotlib.backends.backend_pdf import PdfPages as PDF
  from matplotlib.gridspec import GridSpec
  from matplotlib.patches import Patch
  from matplotlib.lines import Line2D
  from matplotlib import rc  

  mpl.rcParams["text.usetex"] = True
  # Helvetica
  # mpl.rcParams["text.latex.preamble"] = "\\usepackage{amsmath,amssymb,siunitx,helvet}\\usepackage[helvet]{sfmath}"
  # Times New Roman (except for ATLAS logo)
  mpl.rcParams["text.latex.preamble"] = "\\usepackage{newtxtext,newtxmath,siunitx}"
  mpl.rcParams["font.family"] = 'sans-serif'
  mpl.rcParams['font.sans-serif'] = ['Helvetica']

  bin_centers = [(inputs['bin_edges'][i]+inputs['bin_edges'][i+1])/2.0 for i in range(len(inputs['bin_edges'])-1)]
  bin_widths = np.array([(inputs['bin_edges'][i+1]-inputs['bin_edges'][i]) for i in range(len(inputs['bin_edges'])-1)])
  uniform_edges = np.array(list(range(len(inputs['bin_edges']))))
  uniform_centers = np.array([(uniform_edges[i]+uniform_edges[i+1])/2.0 for i in range(len(uniform_edges)-1)])

  with open(inputs['sm_file'], 'r') as stream:
    sm_xsec = yaml.safe_load(stream)['sm xsec']
  
  for ibin, bin in enumerate(inputs['bins']):
    # sm_xsec[bin]['val'] = hyy_vals[ibin]
    # sm_xsec[bin]['err'] = hyy_errs[ibin]
    sm_xsec[bin]['err'] = inputs['sm_relerrs'][ibin] * sm_xsec[bin]['val']

  sm_vals = np.array([sm_xsec[bin]['val'] for bin in inputs['bins']])
  sm_vals = sm_vals / bin_widths
  sm_errs = np.array([sm_xsec[bin]['err'] for bin in inputs['bins']])
  sm_errs = sm_errs / bin_widths
  sm_relerrs = sm_errs / sm_vals
  # print(sm_relerrs)

  with open(inputs['data_file'], 'r') as stream:
    data_fitresult = yaml.safe_load(stream)

  print(data_fitresult['names'])
  print(inputs['pois'])

  data_poi_indices = [data_fitresult['names'].index(poi) for poi in inputs['pois']]
  data_strengths = np.array([data_fitresult['measured'][i] for i in data_poi_indices])
  data_relerrs = np.sqrt(np.array([data_fitresult['covariance'][i][i] for i in data_poi_indices]))
  # data_relerrs = np.zeros(len(data_strengths))
  data_vals = data_strengths * sm_vals

  with open(inputs['smeft_file'], 'r') as stream:
    smeft_fitresult = yaml.safe_load(stream)

  with open(inputs['wc_file'],'r') as stream:
    wc_fitresult = yaml.safe_load(stream)

  wcs = wc_fitresult['names']
  wc_vals = np.array(wc_fitresult['measured'])
  wc_vals_rounded = [round(wc_vals[i], inputs['wc_precision'].get(wcs[i],3)) for i in range(len(wcs))]
  for iwc, wc_val_rounded in enumerate(wc_vals_rounded):
    if wc_val_rounded==0.0: wc_vals_rounded[iwc] = 0.0
  wc_errs_up = np.array([round(wc_fitresult['error'][i][0], inputs['wc_precision'].get(wcs[i],3)) for i in range(len(wcs))])
  wc_errs_dn = np.array([round(wc_fitresult['error'][i][1], inputs['wc_precision'].get(wcs[i],3)) for i in range(len(wcs))])

  # wc_vals = np.array([float(wc_fitresult[wc].split(' ')[0]) for wc in inputs['wcs']])
  # wc_errs_up = np.array([float(wc_fitresult[wc].split(' ')[1]) for wc in inputs['wcs']])
  # wc_errs_dn = np.array([float(wc_fitresult[wc].split(' ')[2]) for wc in inputs['wcs']])

  smeft_poi_indices = [smeft_fitresult['names'].index(bin) for bin in inputs['bins']]
  smeft_strengths = np.array([smeft_fitresult['measured'][i] for i in smeft_poi_indices])
  smeft_relerrs = np.sqrt(np.array([smeft_fitresult['covariance'][i][i] for i in smeft_poi_indices]))
  smeft_vals = smeft_strengths * sm_vals

  # chi2 calculations
  print(data_relerrs * sm_vals)
  print(smeft_relerrs * sm_vals)
  print(sm_vals)
  chi2_sm = get_chi2(sm_vals, data_vals, np.sqrt(np.square(sm_relerrs * sm_vals) + np.square(data_relerrs * sm_vals)))
  chi2_smeft = get_chi2(smeft_vals, data_vals, np.sqrt(np.square(smeft_relerrs * sm_vals) + np.square(data_relerrs * sm_vals)))

  prob_sm = ROOT.TMath.Prob(chi2_sm, len(data_vals))
  prob_smeft = ROOT.TMath.Prob(chi2_smeft, 3)
  print(chi2_sm, chi2_smeft)
  print(prob_sm, prob_smeft)

  # print(chi2_data, chi2_smeft)

  # print(len(smeft_vals))
  # print(len(bin_widths))
  # print(smeft_relerrs)

  with PDF('smeft_pth_postfit.pdf') as pdf:

    fig, axs = plt.subplots(2, 1, figsize=(6,6), gridspec_kw = {'height_ratios': [3,1]}, sharex=True, sharey=False, constrained_layout=True)
    # fig.set_constrained_layout_pads(h_pad=1e-5, hspace=1e-5)

    main_ax = axs[0]
    # main_ax.set_yscale('log')
    main_ax.tick_params(direction='in')
    main_ax.tick_params(axis="x", bottom=True, top=True, labelbottom=False, labeltop=False)
    main_ax.tick_params(axis="y", left=True, right=True, labelleft=True, labelright=False)

    main_ax.set_ylabel('$\\mathrm{d}\\sigma / \\mathrm{d}$'+inputs['observable'] +' '+'$\\textrm{[fb/GeV]}$', loc='top', fontsize='x-large')
    main_ax.set_xlim(uniform_edges[0], uniform_edges[-1])
    if inputs.get('logy', False):
      main_ax.set_yscale('log')
      main_ax.set_ylim(np.min(sm_vals)*0.1,np.max(sm_vals)*100.0)
    else:
      main_ax.set_ylim(0.0,np.max(data_vals)*1.6)

    # sm
    main_ax.stairs(values=sm_vals, edges=uniform_edges, color = sm_color)
    for ival, sm_val in enumerate(sm_vals):
      main_ax.fill_between(x=[uniform_edges[ival],uniform_edges[ival]+1.0], y1 = [sm_val+(sm_vals[ival]*sm_relerrs[ival])]*2, y2 = [sm_val-(sm_vals[ival]*sm_relerrs[ival])]*2, color=smerr_color, alpha=0.50, linewidth=0.)

    # smeft
    main_ax.hlines(y=smeft_vals, xmin = uniform_edges[:-1]+0.6, xmax = uniform_edges[1:]-0.1, linestyle='solid', color=smeft_color)
    for ival, smeft_val in enumerate(smeft_vals):
      main_ax.fill_between(x=[uniform_edges[ival]+0.6,uniform_edges[ival]+0.9], y1 = [smeft_val+(sm_vals[ival]*smeft_relerrs[ival])]*2, y2 = [smeft_val-(sm_vals[ival]*smeft_relerrs[ival])]*2, alpha=0.25, color=smeft_color, linewidth=0.)

    # data
    data_errorbars = main_ax.errorbar(x=uniform_centers, y=data_vals, xerr=None, yerr=sm_vals*data_relerrs, ls='none', color=data_color)
    data_points = main_ax.scatter(x=uniform_centers, y=data_vals, color=data_color, zorder=10)

    # ATLAS labels
    plt.gcf().text(0.15, 0.875,
      "$\\textbf{\\textit{ATLAS}}$ Preliminary\n"+inputs['process']+"\n$\\sqrt{s} = \\SI{13}{\\TeV},\\ \\SI{139}{\\femto\\barn\\tothe{-1}}$",
      color='black', fontsize='large'
    )

    legend_elements = [ 
                        (data_points, data_errorbars),
                        (Line2D([0], [0], color=sm_color, lw=1,),Patch(facecolor=smerr_color, edgecolor='none', alpha=0.50)),
                        (Line2D([0], [0], color=smeft_color, lw=1) ,Patch(facecolor=smeft_color, edgecolor='none', alpha=0.25)),
                      ]
    # sm_label='$\\textrm{{SM (pre-fit)}}$\n$\\color{{red}}P(\\chi^2 = {chi2:.1f},n_{{\\textrm{{dof}}}} = {ndf:.0f}) = {prob:.1f}\\,\\%$'.format(chi2=chi2_sm, ndf=len(sm_vals), prob=prob_sm*100)
    # smeft_label = '$\\textrm{{SMEFT (post-fit)}}$\n$\\color{{purple}}P(\\chi^2 = {chi2:.1f},n_{{\\textrm{{dof}}}} = {ndf:.0f}) = {prob:.1f}\\,\\%$'.format(chi2=chi2_smeft, ndf=len(wc_vals), prob=prob_smeft*100)
    sm_label='$\\textrm{{SM (pre-fit)}}$'
    smeft_label = '$\\textrm{{SMEFT (post-fit)}}$'
    if inputs.get('logy', False):
      main_ax.legend(legend_elements, ['$\\textrm{Data}$',sm_label,smeft_label], loc='lower left', bbox_to_anchor=(0.05,0.40),frameon=False, fontsize='large')
    else:
      main_ax.legend(legend_elements, ['$\\textrm{Data}$',sm_label,smeft_label], loc='lower left', bbox_to_anchor=(0.5,0.75),frameon=False, fontsize='large')

    # WC values
    wc_value_lines = []
    for iwc, wc in enumerate(wcs):
      # wc_value_lines.append(inputs['wc_label'].get(wc,wc)+"$ = $"+'${:g}$'.format(wc_vals[iwc])+"$^{{+{:g}}}_{{{:g}}}$".format(wc_errs_up[iwc],wc_errs_dn[iwc]))
      wc_value_lines.append(inputs['wc_label'].get(wc,wc)+"$\\ =\\ $"+"${:.{}f}$".format(wc_vals_rounded[iwc],inputs['wc_precision'].get(wc,3))+"$^{{+{}}}_{{{}}}$".format(wc_errs_up[iwc],wc_errs_dn[iwc]))
    if inputs.get('logy', False):
      plt.gcf().text(0.2, 0.60, '\n'.join(wc_value_lines), color='black', fontsize='large')
    else:
      plt.gcf().text(0.6, 0.725, '\n'.join(wc_value_lines), color='black', fontsize='large')

    # chi2 & prob labels
    # plt.gcf().text(0.55,0.6,'$\\textrm{{SM: }} \\chi^2 = {chi2:.1f}, P(\\chi^2,n_{{\\textrm{{dof}}}}) = {prob:.1f}\\,\\%$'.format(chi2=chi2_sm, prob=prob_sm*100),color=sm_color)
    # plt.gcf().text(0.55,0.57,'$\\textrm{{SMEFT: }} \\chi^2 = {chi2:.1f}, P(\\chi^2,n_{{\\textrm{{dof}}}}) = {prob:.1f}\\,\\%$'.format(chi2=chi2_smeft, prob=prob_smeft*100),color=smeft_color)

    # main_ax.fill_between(x=uniform_edges[:-1], y1 = smeft_vals+(sm_vals*smeft_relerrs), y2 = smeft_vals-(sm_vals*smeft_relerrs), step='post', alpha=0.25, color=smeft_color, linewidth=0.)

    ratio_ax = axs[1]
    ratio_ax.tick_params(direction='in')
    ratio_ax.tick_params(axis="x", bottom=True, top=True, labelbottom=True, labeltop=False)
    ratio_ax.tick_params(axis="y", left=True, right=True, labelleft=True, labelright=False)
    ratio_ax.set_xticks(uniform_edges)
    ratio_ax.set_xticklabels(['$\\textrm{}$'.format(bin_edge) for bin_edge in inputs['bin_edges']], fontsize='small')
    ratio_ax.set_xlabel(inputs['observable']+' '+'$\\textrm{[GeV]}$', loc='right', fontsize='x-large')
    ratio_ax.set_ylabel('$\\textrm{Ratio to SM}$', fontsize='x-large')
    # ratio_ax.set_ylabel('$\\textrm{Ratio to Data}$', fontsize='x-large')
    ratio_ax.set_ylim(0.0,2.5)

    # sm
    ratio_ax.stairs(values=sm_vals/sm_vals, edges=uniform_edges, color=sm_color)
    ratio_ax.fill_between(x=uniform_edges[:-1], y1 = 1.0+sm_relerrs, y2 = 1.0-sm_relerrs, step='post', color=smerr_color, linewidth=0.)
    for ival, sm_val in enumerate(sm_vals):
      ratio_ax.fill_between(x=[uniform_edges[ival],uniform_edges[ival]+1.0], y1 = np.array([sm_val+(sm_vals[ival]*sm_relerrs[ival])]*2)/sm_vals[ival], y2 = np.array([sm_val-(sm_vals[ival]*sm_relerrs[ival])]*2)/sm_vals[ival], color=smerr_color, alpha=0.5, linewidth=0.)

    # smeft
    ratio_ax.hlines(y=smeft_vals/sm_vals, xmin = uniform_edges[:-1]+0.6, xmax = uniform_edges[1:]-0.1, linestyle='solid', color=smeft_color)
    for ival, smeft_val in enumerate(smeft_vals):
      ratio_ax.fill_between(x=[uniform_edges[ival]+0.6,uniform_edges[ival]+0.9], y1 = np.array([smeft_val+(sm_vals[ival]*smeft_relerrs[ival])]*2)/sm_vals[ival], y2 = np.array([smeft_val-(sm_vals[ival]*smeft_relerrs[ival])]*2)/sm_vals[ival], alpha=0.25, color=smeft_color, linewidth=0.)

    # data
    data_vals_for_ratio = []
    data_relerrs_for_ratio = []
    sm_vals_for_ratio = []
    uniform_centers_for_ratio = []
    for ipt, data_val in enumerate(data_vals):
      if data_vals[ipt] / sm_vals[ipt] > 2.5 or sm_vals[ipt] < 0.0:
        continue
      if data_vals[ipt] / sm_vals[ipt] < 0.0 and sm_vals[ipt] > 0.0:
        continue
      data_vals_for_ratio.append(data_vals[ipt])
      data_relerrs_for_ratio.append(data_relerrs[ipt])
      sm_vals_for_ratio.append(sm_vals[ipt])
      uniform_centers_for_ratio.append(uniform_centers[ipt])
    data_vals_for_ratio = np.array(data_vals_for_ratio)
    data_relerrs_for_ratio  = np.array(data_relerrs_for_ratio)
    sm_vals_for_ratio = np.array(sm_vals_for_ratio)
    uniform_centers_for_ratio = np.array(uniform_centers_for_ratio)

    ratio_ax.errorbar(x=uniform_centers_for_ratio, y=data_vals_for_ratio/sm_vals_for_ratio, xerr=None, yerr=data_relerrs_for_ratio, ls='none', color=data_color)
    ratio_ax.scatter(x=uniform_centers_for_ratio, y=data_vals_for_ratio/sm_vals_for_ratio, color=data_color, zorder=10)

    for ipt, data_val in enumerate(data_vals):
      # draw upper arrow
      if data_vals[ipt] / sm_vals[ipt] > 2.5 or sm_vals[ipt] < 0.0:
        ratio_ax.arrow(x=uniform_centers[ipt],y=1.5, dx=0.0, dy=+0.5, length_includes_head=True, color='grey', width=0.05)
      if data_vals[ipt] / sm_vals[ipt] < 0.0 and sm_vals[ipt] > 0.0:
        ratio_ax.arrow(x=uniform_centers[ipt],y=0.5, dx=0.0, dy=-0.5, length_includes_head=True, color='grey', width=0.05)

    pdf.savefig(fig)
    plt.close()

