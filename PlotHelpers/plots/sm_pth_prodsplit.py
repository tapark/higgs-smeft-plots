sm_color = 'red'

def plot(inputs):

  import yaml
  import collections
  import itertools

  import ROOT
  import math
  import numpy as np

  import matplotlib.pyplot as plt
  import matplotlib.colors as colors
  from matplotlib.ticker import AutoMinorLocator
  from matplotlib.backends.backend_pdf import PdfPages as PDF
  from matplotlib.gridspec import GridSpec
  from matplotlib.patches import Patch
  from matplotlib.lines import Line2D
  from matplotlib import rc  

  plt.rcParams["text.usetex"] = True
  plt.rcParams["text.latex.preamble"] = "\\usepackage{newtxtext,newtxmath,siunitx}"
  plt.rcParams["font.family"] = 'sans-serif'
  plt.rcParams['font.sans-serif'] = ['Helvetica']

  prod_modes = inputs['prod_modes']

  bin_names = inputs['bins']
  bin_centers = np.array([(inputs['bin_edges'][i]+inputs['bin_edges'][i+1])/2.0 for i in range(len(inputs['bin_edges'])-1)])
  bin_widths = np.array([(inputs['bin_edges'][i+1]-inputs['bin_edges'][i]) for i in range(len(inputs['bin_edges'])-1)])
  uniform_edges = np.array(list(range(len(inputs['bin_edges']))))
  uniform_centers = np.array([(uniform_edges[i]+uniform_edges[i+1])/2.0 for i in range(len(uniform_edges)-1)])

  sm_pred = {}
  for prod_mode in prod_modes:

    with open(inputs['param_file'][prod_mode],'rb') as param_file:
      prod_param = yaml.safe_load(param_file)

    sm_pred[prod_mode] = np.zeros(len(bin_names))
    for ibin, bin_name in enumerate(bin_names):
      sm_pred[prod_mode][ibin] = prod_param['sm xsec'][bin_name]['val'] / bin_widths[ibin]

  # sort prod_modes
  prod_tots = []
  for prod_mode in prod_modes:
    prod_tots.append(np.sum(sm_pred[prod_mode]))

  prod_modes_sorted = [prod_mode for _,prod_mode in sorted(zip(prod_tots,prod_modes))]
  prod_modes_descending = prod_modes_sorted[::-1]

  with PDF('sm_pth.pdf') as pdf:

    plt.tight_layout()
    fig, ax= plt.subplots(figsize=(6,6))

    # sm
    total_stack = np.zeros(len(bin_names))
    for iprod,prod_mode in enumerate(prod_modes_sorted):
      ax.stairs(values=sm_pred[prod_mode]+total_stack, edges=uniform_edges, color = inputs['prod_color'][prod_mode], fill=True, zorder=-iprod)
      total_stack += sm_pred[prod_mode]
      # for ival, sm_val in enumerate(sm_pred):
      #   ax.fill_between(x=[uniform_edges[ival],uniform_edges[ival]+1.0], y1 = [sm_val+(sm_pred[ival]*sm_relerrs[ival])]*2, y2 = [sm_val-(sm_pred[ival]*sm_relerrs[ival])]*2, color='lightgrey', linewidth=0.)

    total_stack_errup = total_stack + total_stack*np.array(inputs['sm_relerrs'])
    total_stack_errdn = total_stack - total_stack*np.array(inputs['sm_relerrs'])

    for ibin, bin_name in enumerate(bin_names):
      ax.fill_between(x=[uniform_edges[ibin],uniform_edges[ibin]+1.0], y1 = [total_stack_errdn[ibin]]*2, y2 = [total_stack_errup[ibin]]*2, facecolor='white', alpha=0.0, edgecolor='grey', linewidth=0.5, hatch='////')

    ax.tick_params(direction='in')

    ax.tick_params(axis="y", left=True, right=True, labelleft=True, labelright=False)
    ax.set_ylabel('$\\mathrm{d}\\sigma / \\mathrm{d}$'+inputs['observable'] +' '+'$\\textrm{[fb/GeV]}$', loc='top', fontsize='x-large')
    if inputs.get('logy', False):
      ax.set_yscale('log')
      ax.set_ylim(np.min(sm_pred[prod_modes_sorted[0]])*0.5, np.max(total_stack)*100)
    # else:
      # ax.set_ylim(np.max(total_stack)*1.5)

    ax.tick_params(axis="x", bottom=True, top=True, labelbottom=True, labeltop=False)
    ax.set_xlabel(inputs['observable']+' '+'$\\textrm{[GeV]}$', loc='right', fontsize='x-large')
    ax.set_xlim(uniform_edges[0], uniform_edges[-1])
    ax.set_xticks(uniform_edges)
    ax.set_xticklabels(['$\\textrm{{{}}}$'.format(bin_edge) for bin_edge in inputs['bin_edges']], fontsize='x-small')

    # legend
    legend_entries = []
    for prod_mode in prod_modes_descending:
      # legend_entries.append(Line2D([0], [0], color=inputs['prod_color'][prod_mode], lw=1))
      legend_entries.append(Patch(facecolor=inputs['prod_color'][prod_mode], edgecolor=inputs['prod_color'][prod_mode]))
    legend_entries.append(Patch(facecolor='white', edgecolor='grey', hatch='////', linewidth=0.5))
    ax.legend(legend_entries, [inputs['prod_label'][prod_mode] for prod_mode in prod_modes_descending] + ['$\\textrm{SM unc. (pre-fit)}$'], ncol=2, loc='upper left', bbox_to_anchor=(0.05,0.95), frameon=False, fontsize='small')

    # labels
    plt.gcf().text(0.6, 0.75,
      "$\\textbf{\\textit{ATLAS}}$ Preliminary\n"+inputs['process']+"\n$\\sqrt{s} = \\SI{13}{\\TeV},\\ \\SI{139}{\\femto\\barn\\tothe{-1}}$",
      color='black', fontsize='large'
    )

    pdf.savefig(fig, bbox_inches='tight')
    plt.close()

