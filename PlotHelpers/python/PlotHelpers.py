import copy
import collections

import ROOT

# get rid of X error bars 
# ROOT.gStyle.SetErrorX(0.001)
# get rid of error bar caps
ROOT.gStyle.SetTextFont(42)
ROOT.gStyle.SetTextSize(0.04)
ROOT.gStyle.SetEndErrorSize(0.001)
# do not display any of the standard histogram decorations
ROOT.gStyle.SetOptTitle(0)
#ROOT.gStyle.SetOptStat(1111)
ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetOptFit(1111)
ROOT.gStyle.SetOptFit(0)
# put tick marks on top and RHS of plots
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
# legend
ROOT.gStyle.SetLegendBorderSize(0)
# set style
ROOT.gStyle.cd()

def figure(skip=False, **settings):
  pass

class Figure():

  def __init__(self, width=800, height=600, margins={}):
    self.width = width
    self.height = height
    self.lmargin = margins.get('left', 120)
    self.rmargin = margins.get('right', 40)
    self.tmargin = margins.get('top', 40)
    self.bmargin = margins.get('bottom', 120)
    self.canvas = ROOT.TCanvas('c','c',width,height)
    self.canvas.SetWindowSize(width + (width - self.canvas.GetWw()), height + (height - self.canvas.GetWh()))
    self.canvas.SetLeftMargin(self.lmargin/float(self.width))
    self.canvas.SetRightMargin(self.rmargin/float(self.width))
    self.canvas.SetTopMargin(self.tmargin/float(self.height))
    self.canvas.SetBottomMargin(self.bmargin/float(self.height))
    self.name = None
    self.opened = False

  def open(self, name='plot', ext='pdf'):
    self.name = name
    self.opened = True
    self.extension = ext
    self.canvas.Print(name+'.'+self.extension+'[')

  def save(self, plot, leg=None, name='', ext='pdf'):
    plot.on_figure(self)
    if leg: leg.on_figure(self)
    self.canvas.cd()

    # TEMP: ATLAS label
    ltx = ROOT.TLatex()
    ltx.DrawLatex(0.20,0.875,"#it{#bf{ATLAS}} Preliminary");
    # ltx.DrawLatex(0.20,0.825,"#it{H}#rightarrow#it{WW}*#rightarrow#it{e#nu#mu#nu}");
    ltx.DrawLatex(0.20,0.825,"#it{H}#rightarrow#it{ZZ}*#rightarrow#it{4l}");
    ltx.DrawLatex(0.20,0.775,"#sqrt{#it{s}} = 13 TeV, 139 fb^{-1}");

    if self.opened:
      if self.name:
        self.canvas.Print(self.name+'.'+self.extension)
      else:
        self.canvas.Print(name+'.'+self.extension)
    else:
      self.extension = ext
      self.canvas.Print(name+'.'+self.extension)

  def close(self):
    self.canvas.Print(self.name+'.'+self.extension+']')
    self.name = None
    self.opeend = False

class Plot(object):

  def __init__(self, xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0):
    self.ax = ROOT.TH1F('ax','ax',100,xmin,xmax)
    self.ax.GetYaxis().SetRangeUser(ymin,ymax)
    self.data = {}

    self.xtitle_offset = 2.0
    self.ytitle_offset = 2.0

  def xtitle(self, title='', offset=1.0):
    self.ax.GetXaxis().SetTitle(title)
    self.xtitle_offset = offset
    pass

  def ytitle(self, title='', offset=1.0):
    self.ax.GetYaxis().SetTitle(title)
    self.ytitle_offset = offset
    pass

  def xticks(self, primary, secondary, tertiary, length):
    pass

  def yticks(self, primary, secondary, tertiary, length):
    pass

  def on_figure(self, fig):
    pass

class Ratio(Plot):

  @staticmethod
  def divide_without_error(numerator, denominator):
    for ix in range(numerator.GetNbinsX()):
      if denominator.GetBinContent(ix+1):
        rval = numerator.GetBinContent(ix+1) / denominator.GetBinContent(ix+1)
        rerr = numerator.GetBinError(ix+1) / denominator.GetBinContent(ix+1)
      else:
        rval = 0.0
        rerr = 0.0
      numerator.SetBinContent(ix+1, rval)
      numerator.SetBinError(ix+1, rerr)

  def __init__(self, xbins=[], xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0, rmin=0.0, rmax=2.0, subfrac=0.5):
    super(Ratio,self).__init__(xmin, xmax, ymin, ymax)

    self.rtitle_offset = 2.0
    self.subfrac = subfrac

    if xbins:
      self.ax = ROOT.TH2F('ax','ax',len(xbins),0,len(xbins),100,ymin,ymax)
      self.subax = ROOT.TH1F('subax','subax',len(xbins),0,len(xbins))
      for ix,xbin in enumerate(xbins):
        self.ax.GetXaxis().SetBinLabel(ix+1, xbin)
        self.subax.GetXaxis().SetBinLabel(ix+1, xbin)
    else:
      self.ax = ROOT.TH1F('ax','ax',10,xmin,xmax)
      self.subax = ROOT.TH1F('subax','subax',10,xmin,xmax)

    self.ax.GetYaxis().SetRangeUser(ymin,ymax)
    self.subax.GetYaxis().SetRangeUser(rmin,rmax)

    self.denominator = None
    self.numerators = []
    self.main_etc = []
    self.sub_etc = []

  def set_denominator(self, data):
    self.denominator = data

  def add_numerator(self, data):
    self.numerators.append(data)

  def add_main(self, data):
    self.main_etc.append(data)

  def add_sub(self, data):
    self.sub_etc.append(data)

  def xtitle(self, title='', offset=1.0):
    self.subax.GetXaxis().SetTitle(title)
    self.xtitle_offset = offset
    pass

  def rtitle(self, title='', offset=1.0):
    # axis titles
    self.subax.GetYaxis().SetTitle(title)
    self.rtitle_offset = offset
    pass

  def rticks(self, primary=10, secondary=5, tertiary=0, length=12):
    self.subax.GetYaxis().SetNdivisions(primary + secondary*100 + tertiary*10000)
    pass

  def plot(self, data):
    data.on_plot(self)

  def on_figure(self, fig):

    # draw pads on canvas

    fig.canvas.cd()
    self.pad = ROOT.TPad('pad','pad',0,0,1,1)
    self.pad.SetFillStyle(4000)
    self.pad.Draw()
    self.pad.SetLeftMargin(fig.lmargin/float(fig.width))
    self.pad.SetRightMargin(fig.rmargin/float(fig.width))

    fig.canvas.cd()
    self.subpad = ROOT.TPad('subpad','subpad',0,0,1,1)
    self.subpad.SetFillStyle(4000)
    self.subpad.Draw()
    self.subpad.SetLeftMargin(fig.lmargin/float(fig.width))
    self.subpad.SetRightMargin(fig.rmargin/float(fig.width))

    # split vertically
    fig.canvas.cd()
    self.pad.SetTopMargin(fig.tmargin/float(fig.height))
    self.subpad.SetBottomMargin(fig.bmargin/float(fig.height))
    eff_height = (fig.height - fig.tmargin - fig.bmargin)
    self.pad.SetBottomMargin((fig.bmargin+eff_height*self.subfrac)/fig.height)
    self.subpad.SetTopMargin((fig.tmargin+eff_height*(1-self.subfrac))/fig.height)

    # erase overlapping axis labels
    self.ax.GetXaxis().SetLabelSize(0)
    self.ax.GetXaxis().SetTitleSize(0)
    # self.subax.GetYaxis().ChangeLabel(-1,-1,0)

    # normalize x-tick lengths

    # draw comparisons
    fig.canvas.cd()
    self.pad.cd()
    self.ax.Draw('axis')

    xtitle_offset_scale = min(fig.width/float(fig.height),1.0)
    self.subax.GetXaxis().SetTitleOffset(self.xtitle_offset * xtitle_offset_scale)
    ytitle_offset_scale = min(fig.height/float(fig.width),1.0)
    self.ax.GetYaxis().SetTitleOffset(self.ytitle_offset * ytitle_offset_scale)

    for numerator in self.numerators:
      self.plot(numerator)
    self.plot(self.denominator)

    for misc in self.main_etc:
      self.plot(misc)

    # draw ratios
    fig.canvas.cd()
    self.subpad.cd()
    self.subax.Draw('axis')

    # normalize axis title offsets

    self.subax.GetYaxis().SetTitleOffset(self.rtitle_offset * ytitle_offset_scale)
    self.subax.GetYaxis().CenterTitle(ROOT.kTRUE)

    self.comparisons = []

    self.baseline = copy.deepcopy(self.denominator)
    self.divide_without_error(self.baseline.hist, self.baseline.hist)
    self.plot(self.baseline)

    for numerator in self.numerators:
      comparison = copy.deepcopy(numerator)
      self.divide_without_error(comparison.hist, self.denominator.hist)
      self.comparisons.append(comparison)
      self.plot(comparison)

    for misc in self.sub_etc:
      self.plot(misc)

class Legend(object):

  def __init__(self, x, y, width=200, height=150, ncols=1):
    self.xpos = x
    self.ypos = y
    self.width = width
    self.height = height
    self.ncols = ncols
    self.entries = []

  def add_entry(self, data):
    self.entries.append(data)

  def on_figure(self, fig):

    fig.canvas.cd()

    x1, x2 = (self.xpos - self.width/2.0) / fig.width, (self.xpos + self.width/2.0) / fig.width
    y1, y2 = (self.ypos - self.height/2.0) / fig.height, (self.ypos + self.height/2.0) / fig.height

    print(x1, x2, y1, y2)

    self.tleg = ROOT.TLegend(x1,y1, x2,y2)
    self.tleg

    for entry in self.entries:
      entry.on_legend(self)
    
    self.tleg.Draw('same')

class Observation(object):

  def __init__(self, hist, label='Observation', marker={}, line={}, fill={}):
    self.hist = hist
    self.label = label
    self.marker = {'color' : marker.get('color',ROOT.kBlack), 'size' : marker.get('size',1.2), 'style': marker.get('style', ROOT.kFullCircle) }
    self.line = {'color' : line.get('color',ROOT.kBlack), 'width' : line.get('width',2.0), 'style' : line.get('style',ROOT.kSolid) }
    self.fill = {'color' : fill.get('color',ROOT.kRed), 'alpha': fill.get('alpha',1.0 if 'color' in fill else 0.0)}

  def on_plot(self, plt):
    self.hist.SetMarkerColor(self.marker['color'])
    self.hist.SetMarkerSize(self.marker['size'])
    self.hist.SetMarkerStyle(self.marker['style'])
    self.hist.SetLineColor(self.line['color'])
    self.hist.SetLineWidth(int(self.line['width']))
    self.hist.SetLineStyle(self.line['style'])
    self.hist.Draw('p e1 same')

  def on_legend(self, leg):
    pass

class Prediction(Observation):

  def __init__(self, hist, label='Prediction', marker={}, line={}, fill={}):
    super(Prediction,self).__init__(hist, label, marker, line, fill)
    # self.hist = hist
    # self.label = label
    # self.marker = {'color' : marker.get('color',ROOT.kBlack), 'size' : marker.get('size',1.2) }
    # self.line = {'color' : line.get('color',ROOT.kBlack), 'width' : line.get('width',2.0), 'style' : line.get('style',ROOT.kSolid) }
    # self.fill = {'color' : fill.get('color',ROOT.kRed), 'alpha': fill.get('alpha',1.0 if 'color' in fill else 0.0)}

  def on_plot(self, plt):
    # hist line
    # disable marker
    self.hist.SetMarkerStyle(0)
    # enable line
    self.hist.SetLineWidth(int(self.line['width']))
    self.hist.SetLineStyle(self.line['style'])
    self.hist.SetLineColor(self.line['color'])
    # disable fill
    self.hist.SetFillColorAlpha(ROOT.kWhite,0.0)
    # error boxes
    self.error = copy.deepcopy(self.hist)
    # disable marker
    self.error.SetMarkerStyle(0)
    # disable line
    self.error.SetLineWidth(0)
    # enable fill
    self.error.SetFillColorAlpha(self.fill['color'], self.fill['alpha'])
    # draw hist & error separately
    self.error.Draw('e2 same')
    self.hist.Draw('hist ][ same')

  def on_legend(self, leg):
    self.entry = copy.deepcopy(self.hist)
    self.entry.SetFillColorAlpha(self.fill['color'], self.fill['alpha'])
    leg.tleg.AddEntry(self.entry, self.label, 'lf')
    # disable marker
    # enable line
    # enable fill
    # add legend entry
    pass

class Line(Observation):

  def __init__(self, hist, label='', marker={}, line={}, fill={}):
    super(Line,self).__init__(hist, label, marker, line, fill)
    # self.hist = hist
    # self.label = label
    # self.marker = {'color' : marker.get('color',ROOT.kBlack), 'size' : marker.get('size',1.2) }
    # self.line = {'color' : line.get('color',ROOT.kBlack), 'width' : line.get('width',2.0), 'style' : line.get('style',ROOT.kSolid) }
    # self.fill = {'color' : fill.get('color',ROOT.kRed), 'alpha': fill.get('alpha',1.0 if 'color' in fill else 0.0)}

  def on_plot(self, plt):
    # hist line
    # disable marker
    self.hist.SetMarkerStyle(0)
    # enable line
    self.hist.SetLineWidth(int(self.line['width']))
    self.hist.SetLineStyle(self.line['style'])
    self.hist.SetLineColor(self.line['color'])
    # disable fill
    self.hist.SetFillColorAlpha(ROOT.kWhite,0.0)
    # error boxes
    self.error = copy.deepcopy(self.hist)
    # disable marker
    self.error.SetMarkerStyle(0)
    # disable line
    self.error.SetLineWidth(0)
    # enable fill
    self.error.SetFillColorAlpha(self.fill['color'], self.fill['alpha'])
    # draw hist & error separately
    self.error.Draw('e2 same')
    self.hist.Draw('l same')

  def on_legend(self, leg):
    self.entry = copy.deepcopy(self.hist)
    # disable marker
    # enable line
    # enable fill
    # add legend entry
    pass

from shape_plot import ShapePlot