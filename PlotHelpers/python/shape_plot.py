import copy
import collections

import ROOT

import PlotHelpers
Plot = PlotHelpers.Plot

class ShapePlot(Plot):

  @staticmethod
  def normalize_to_baseline_unity(comparisons, baseline):
    norm = 1.0/baseline.Integral('width')
    baseline.Scale(norm)
    for comp in comparisons:
      comp.Scale(norm)

  @staticmethod
  def normalize_to_unity(hist):
    hist.Scale(1.0/hist.Integral('width'))

  @staticmethod
  def divide_without_error(comparison, baseline):
    for ix in range(comparison.GetNbinsX()):
      if baseline.GetBinContent(ix+1):
        rval = comparison.GetBinContent(ix+1) / baseline.GetBinContent(ix+1)
        rerr = comparison.GetBinError(ix+1) / baseline.GetBinContent(ix+1)
      else:
        rval = 0.0
        rerr = 0.0
      comparison.SetBinContent(ix+1, rval)
      comparison.SetBinError(ix+1, rerr)

  def __init__(self, xbins=[], xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0, rmin=0.0, rmax=2.0, subfrac=0.5):
    super(ShapePlot,self).__init__(xmin, xmax, ymin, ymax)

    self.rtitle_offset = 2.0
    self.subfrac = subfrac

    if xbins:
      self.ax = ROOT.TH2F('ax','ax',len(xbins),0,len(xbins),100,ymin,ymax)
      self.subax = ROOT.TH1F('subax','subax',len(xbins),0,len(xbins))
      for ix,xbin in enumerate(xbins):
        self.ax.GetXaxis().SetBinLabel(ix+1, xbin)
        self.subax.GetXaxis().SetBinLabel(ix+1, xbin)
    else:
      self.ax = ROOT.TH1F('ax','ax',10,xmin,xmax)
      self.subax = ROOT.TH1F('subax','subax',10,xmin,xmax)

    self.ax.GetYaxis().SetRangeUser(ymin,ymax)
    self.subax.GetYaxis().SetRangeUser(rmin,rmax)

    self.baseline = None
    self.comparisons = []
    self.main_etc = []
    self.sub_etc = []

  def set_baseline(self, data):
    self.baseline = data

  def add_comparison(self, data):
    self.comparisons.append(data)

  def add_main(self, data):
    self.main_etc.append(data)

  def add_sub(self, data):
    self.sub_etc.append(data)

  def xtitle(self, title='', offset=1.0):
    self.subax.GetXaxis().SetTitle(title)
    self.xtitle_offset = offset
    pass

  def rtitle(self, title='', offset=1.0):
    # axis titles
    self.subax.GetYaxis().SetTitle(title)
    self.rtitle_offset = offset
    pass

  def rticks(self, primary=10, secondary=5, tertiary=0, length=12):
    self.subax.GetYaxis().SetNdivisions(primary + secondary*100 + tertiary*10000)
    pass

  def plot(self, data):
    data.on_plot(self)

  def on_figure(self, fig):

    # draw pads on canvas
    fig.canvas.cd()
    self.pad = ROOT.TPad('pad','pad',0,0,1,1)
    self.pad.SetFillStyle(4000)
    self.pad.Draw()
    self.pad.SetLeftMargin(fig.lmargin/float(fig.width))
    self.pad.SetRightMargin(fig.rmargin/float(fig.width))

    fig.canvas.cd()
    self.subpad = ROOT.TPad('subpad','subpad',0,0,1,1)
    self.subpad.SetFillStyle(4000)
    self.subpad.Draw()
    self.subpad.SetLeftMargin(fig.lmargin/float(fig.width))
    self.subpad.SetRightMargin(fig.rmargin/float(fig.width))

    # split vertically
    fig.canvas.cd()
    self.pad.SetTopMargin(fig.tmargin/float(fig.height))
    self.subpad.SetBottomMargin(fig.bmargin/float(fig.height))
    eff_height = (fig.height - fig.tmargin - fig.bmargin)
    self.pad.SetBottomMargin((fig.bmargin+eff_height*self.subfrac)/fig.height)
    self.subpad.SetTopMargin((fig.tmargin+eff_height*(1-self.subfrac))/fig.height)

    # erase overlapping axis labels
    self.ax.GetXaxis().SetLabelSize(0)
    self.ax.GetXaxis().SetTitleSize(0)
    # self.subax.GetYaxis().ChangeLabel(-1,-1,0)

    # halve x-axis tick lengths
    self.ax.GetXaxis().SetTickLength(self.ax.GetXaxis().GetTickLength()*0.5)
    self.subax.GetXaxis().SetTickLength(self.subax.GetXaxis().GetTickLength()*0.5)
    # correct (just) y-axis tick length for sub
    # self.subax.GetYaxis().SetTickLength(self.subax.GetYaxis().GetTickLength()/(1.0-self.subfrac - fig.tmargin/float(fig.height) - fig.bmargin/float(fig.height)))
    self.ax.GetYaxis().SetTickLength(self.ax.GetYaxis().GetTickLength()/(1.0-self.subfrac)*0.5)
    self.subax.GetYaxis().SetTickLength(self.subax.GetYaxis().GetTickLength()/(self.subfrac)*0.5)

    # draw comparisons
    fig.canvas.cd()
    self.pad.cd()
    self.ax.Draw('axis')

    xtitle_offset_scale = min(fig.width/float(fig.height),1.0)
    self.subax.GetXaxis().SetTitleOffset(self.xtitle_offset * xtitle_offset_scale)
    ytitle_offset_scale = min(fig.height/float(fig.width),1.0)
    self.ax.GetYaxis().SetTitleOffset(self.ytitle_offset * ytitle_offset_scale)

    main_baseline = copy.deepcopy(self.baseline)
    main_comparisons = []
    for comparison in self.comparisons:
      main_comparisons.append(copy.deepcopy(comparison))

    self.normalize_to_baseline_unity([comp.hist for comp in self.comparisons], self.baseline.hist)

    for comparison in self.comparisons:
      self.plot(comparison)
    self.plot(self.baseline)

    for misc in self.main_etc:
      self.plot(misc)

    # draw ShapePlots
    fig.canvas.cd()
    self.subpad.cd()
    self.subax.Draw('axis')

    # normalize axis title offsets

    self.subax.GetYaxis().SetTitleOffset(self.rtitle_offset * ytitle_offset_scale)
    self.subax.GetYaxis().CenterTitle(ROOT.kTRUE)

    sub_baseline = copy.deepcopy(self.baseline)
    self.divide_without_error(sub_baseline.hist, self.baseline.hist)
    self.sub_baseline = sub_baseline

    sub_comparisons = []
    for comparison in self.comparisons:
      sub_comparison = copy.deepcopy(comparison)
      self.divide_without_error(sub_comparison.hist, self.baseline.hist)
      sub_comparisons.append(sub_comparison)
    self.sub_comparisons = sub_comparisons

    self.plot(self.sub_baseline)
    for sub_comparison in self.sub_comparisons:
      self.plot(sub_comparison)

    for misc in self.sub_etc:
      self.plot(misc)